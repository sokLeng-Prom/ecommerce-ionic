import { Component, Input, OnInit } from '@angular/core';
import { Data, Router } from '@angular/router';
import { DataService } from '../shared/data-service.server.service';
@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.page.html',
  styleUrls: ['./wishlist.page.scss'],
})
export class WishlistPage implements OnInit {
  wishlist = this.dataservice.wishListData;

  constructor(private router: Router, private dataservice: DataService) {
    // this.wishListData = JSON.parse(localStorage.getItem('wishListData')) || [];
  }

  //fire all the time

  ngOnInit() {
    console.log(this.wishlist);
    this.wishlist = this.dataservice.wishListData;
    // this.dataservice.displayWishList();
  }

  // ngOnchanges() {
  //   this.wishlist = this.dataservice.wishListData;
  // }

  loginNavigation() {
    this.router.navigateByUrl('tabs/login');
  }
}

interface Product {
  category: string;
  description: string;
  id: number;
  price: number;
  image: string;
  title: string;
}
