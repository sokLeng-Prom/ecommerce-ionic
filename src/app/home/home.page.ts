import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SortPipe } from '../sort.pipe';
import { Sort } from '@angular/material/sort';
import { DataService } from '../shared/data-service.server.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})

// interface Product { }
export class HomePage implements OnInit {
  /** all variable should declare on top make our code clean */
  filterProductData: Product[];
  productData: Product[];
  searchTerm: string;
  descending: boolean = false;
  order: number;
  column: string = 'name';
  product: any;
  postId;
  // wishListData: Product[];

  constructor(
    private http: HttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dataservice: DataService
  ) {}

  /** ngOnInit : mean the first loading so can do some action like request api for the first time */
  ngOnInit() {
    /** Example 2 api
     * so first time load in this function first and then u can data from to client
     */
    this.inquiryVendor();
  }

  inquiryVendor() {
    const url = environment.url + '/products';
    this.http.get(url).subscribe((data) => {
      //process the json data
      // console.log(data)
      this.productData = data as Product[];
      this.filterProductData = this.productData;
    });

    // console.log(this.wishListData.length);
    // const result = this.wishListData.filter(e => e.includes())
  }

  onClick(id): void {
    this.router.navigateByUrl('/tabs/product-detail/' + id);
  }

  filterSort(event) {
    const key = event.target.value as string;
    console.log('event on change===>', key);
    if (key == 'id') {
      this.productData.sort(function (a, b) {
        return a.id - b.id;
      });
    }
    if (key == 'title') {
      this.productData.sort(function (a, b) {
        if (a.title < b.title) {
          return -1;
        }
        if (a.title > b.title) {
          return 1;
        }
      });
    }
    if (key == 'price') {
      this.productData.sort(function (a, b) {
        return a.price - b.price;
      });
    }
  }

  // search
  filterSearch(event) {
    const textVal = event.target.value.toLowerCase();
    // console.log('Hellowrold===>', textVal);
    const tempProduct = this.filterProductData.filter(function (d) {
      return d.title.toLowerCase().indexOf(textVal) !== -1 || !textVal;
    });
    this.productData = tempProduct;
    // console.log(this.productData)
  }

  loginNavigation() {
    this.router.navigateByUrl('tabs/login');
  }

  addToWishlist(e, i: number) {
    this.dataservice.addingWishlist(e, this.productData[i]);
  }

  inWishList(id: number) {
    const wishList: Product[] = JSON.parse(
      localStorage.getItem('wishListData')
    );
    // console.log(wishList);

    //return boolean
    // .some check each one of it wether true or false
    if (wishList != undefined) {
      return wishList.some((product) => product.id === id);
    }
  }
}

interface Product {
  category: string;
  description: string;
  id: number;
  price: number;
  image: string;
  title: string;
}

interface PostProduct {
  id: string;
  productID: number;
  quantity: number;
}
