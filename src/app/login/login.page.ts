import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { resolveSanitizationFn } from '@angular/compiler/src/render3/view/template';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';
import { defineCustomElements } from '@teamhive/lottie-player/loader';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  constructor(
    private http: HttpClient,
    private router: Router,
    public toastController: ToastController
  ) {}

  inputUserName: string;
  inputPassword: string;

  ngOnInit() {}

  onSubmit() {
    const logInUrl = environment.url + '/auth/login';

    const body = {
      username: this.inputUserName,
      password: this.inputPassword,
    };

    console.log('input==>', body);
    this.http.post(logInUrl, body).subscribe((response: any) => {
      const res = response;
      console.log('res', res);
      if (res['token']) {
        this.confirmationToast();
        // Swal.fire('Confirmation', 'You have already logged in', 'success');
        this.router.navigateByUrl('tabs/home');
      } else {
        this.errorLogin(res['msg']);
        // Swal.fire('Cancelled', res['msg'], 'error');
      }
    });
  }

  async confirmationToast() {
    const toast = await this.toastController.create({
      color: 'dark',
      message: 'You have already logged in.',
      duration: 2000,
    });
    toast.present();
  }

  async errorLogin(msg) {
    const toast = await this.toastController.create({
      color: 'dark',
      message: msg,
      duration: 2000,
    });
    toast.present();
  }

  backNavigation() {
    this.router.navigateByUrl('/tabs/home');
  }
}
