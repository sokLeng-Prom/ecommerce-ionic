import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  constructor(private http: HttpClient, private router: Router) {}

  userDetail: any;

  ngOnInit() {
    this.getUserDetail();
  }

  getUserDetail() {
    const UserDetailUrl = environment.url + '/users/7';
    this.http.get(UserDetailUrl).subscribe((data) => {
      console.log('userdata', data);
      this.userDetail = data;
      console.log(this.userDetail);
    });
  }

  loginNavigation() {
    this.router.navigateByUrl('tabs/login');
  }
}
