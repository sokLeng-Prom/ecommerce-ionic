import { Component, OnInit } from '@angular/core';
import { DataService } from '../shared/data-service.server.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { element } from 'protractor';
import { subscribeOn } from 'rxjs/operators';
import { Router } from '@angular/router';
@Component({
  selector: 'app-deal',
  templateUrl: './deal.page.html',
  styleUrls: ['./deal.page.scss'],
})
export class DealPage implements OnInit {
  constructor(
    private http: HttpClient,
    private dataService: DataService,
    private router: Router
  ) {}

  productCart: any;
  productData: any;
  subtotal: any;
  subtotalArray: number[] = [];
  vat: number;

  ngOnInit() {
    this.getUserCart();
    this.getProductDetail();
    // await this.getProductDetail();
    // const userUrl = environment.url + '/carts/user/2';
    // this.http.get(userUrl).subscribe((data) => {
    //   this.productCart = data;
    //   console.log('productCart', this.productCart);
    // });
  }

  async getUserCart() {
    const userUrl = environment.url + '/carts/user/2';
    await this.http.get(userUrl).subscribe((data) => {
      this.productCart = data;

      console.log('productCart', this.productCart);
    });
  }

  async getProductDetail() {
    const url = environment.url + '/products/';
    await this.http.get(url).subscribe((data) => {
      //process the json data
      this.productData = data;
      this.displayProduct(data);
    });
  }

  productArray = [];
  displayProduct(data) {
    for (let i = 0; i < this.productCart[0].products.length; i++) {
      const cardProductId = this.productCart[0].products[i].productId;
      for (let index = 0; index < data.length; index++) {
        const productId = data[index].id;
        /**
         * Compare ID from card product
         */
        if (cardProductId === productId) {
          const obj = {
            id: data[index].id,
            category: data[index].category,
            description: data[index].description,
            image: data[index].image,
            price: data[index].price,
            title: data[index].title,
            quantity: 1,
          };
          this.productArray.push(obj);
        }
      }
    }
    this.totalPrice();
  }

  decreaseQuantity(index) {
    if (this.productArray[index].quantity > 1) {
      --this.productArray[index].quantity;
    }
    this.totalPrice();
  }

  increaseQuantity(index) {
    ++this.productArray[index].quantity;

    this.totalPrice();
  }

  totalPrice() {
    for (let i = 0; i < this.productArray.length; i++) {
      this.subtotalArray[i] =
        this.productArray[i].quantity * this.productArray[i].price;
    }
    this.subtotal = this.subtotalArray.reduce((start, end) => start + end);
  }

  loginNavigation() {
    this.router.navigateByUrl('tabs/login');
  }

  deleteItem(i) {
    console.log(i);
    this.subtotal = 0;
    this.productArray.splice(i, 1);
    console.log(this.productArray.length);
    // console.log(this.subtotal);
    for (let i = 0; i < this.productArray.length; i++) {
      this.subtotalArray[i] =
        this.productArray[i].quantity * this.productArray[i].price;
    }
    for (let i = 0; i < this.productArray.length; i++) {
      this.subtotal += this.subtotalArray[i];
      console.log(this.subtotal);
    }
  }
}

interface CartProduct {
  id: number;
  userId: number;
  date: Date;
}
