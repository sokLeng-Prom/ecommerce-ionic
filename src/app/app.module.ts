import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchPipe } from './search.pipe';
import { SortPipe } from './sort.pipe';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { Firebase } from '@ionic-native/firebase/ngx';
import { FcmService } from './shared/fcm.service';

const firebaseConfig = {
  apiKey: 'AIzaSyBxBaL8pl--NXEWq-jtrNicJSLmGkRr7KU',
  authDomain: 'ecommerce-ionic-64729.firebaseapp.com',
  projectId: 'ecommerce-ionic-64729',
  storageBucket: 'ecommerce-ionic-64729.appspot.com',
  messagingSenderId: '1006264434765',
  appId: '1:1006264434765:web:55af2bf7ac304e46addc04',
  measurementId: 'G-TZX0RGQBF3',
};

@NgModule({
  declarations: [AppComponent, SearchPipe, SortPipe],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
  ],

  providers: [
    Firebase,
    FcmService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
