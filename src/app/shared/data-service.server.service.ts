import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  productId: number;
  qty: number;
  wishListData: Product[] =
    JSON.parse(localStorage.getItem('wishListData')) || [];

  /**
   *
   * @param Object From Client
   */
  createCart(obj) {
    console.log(obj, '====>object');
    const url = environment.url + '/carts';
    // this.product.push(obj);
    const body = {
      userId: obj.userId,
      date: 2021 - 8 - 17,
      products: [
        {
          productId: obj.productId,
          quantity: obj.qty,
        },
      ],
    };
    console.log('body==>', body);
    return new Promise((resolve, reject) => {
      this.http.post(url, body).subscribe((response: any) => {
        // getp rodcut
        const res = response;
        console.log('res', res);
        // this.getProduct;
        resolve(res);
      }, reject);
    });
  }

  addingWishlist(e, product) {
    const heart = e.target.querySelector('#wishList-heart');
    // console.log(heart);
    this.wishListData = JSON.parse(localStorage.getItem('wishListData')) || [];

    // if else
    if (heart.getAttribute('name') === 'heart-circle-outline') {
      heart.setAttribute('name', 'heart-circle');
      this.wishListData.push(product);
      console.log(this.wishListData);
      localStorage.setItem('wishListData', JSON.stringify(this.wishListData));
    } else {
      heart.setAttribute('name', 'heart-circle-outline');

      //find index , compare id then cut it out
      this.wishListData.splice(
        this.wishListData.findIndex((e) => e.id === product.id),
        1
      );

      console.log(this.wishListData);
    }
    // javascript dom

    // get data from localStorage
    /**
     * if undefined use []
     */

    // save the old and new data in local Storage
    localStorage.setItem('wishListData', JSON.stringify(this.wishListData));
  }
}

interface PostProduct {
  id: any;
  productID: number;
  quantity: number;
}

interface Product {
  category: string;
  description: string;
  id: number;
  price: number;
  image: string;
  title: string;
}
