import { Component } from '@angular/core';
import { Data, Router } from '@angular/router';
import { DataService } from '../shared/data-service.server.service';
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss'],
})
export class TabsPage {
  // wishlist = this.dataservice.wishListData;
  constructor(private router: Router, private dataservice: DataService) {}

  backButton() {
    this.router.navigateByUrl('/tabs/home');
  }

  // displayWishList() {
  //   this.wishlist = this.dataservice.wishListData;
  // }
}
