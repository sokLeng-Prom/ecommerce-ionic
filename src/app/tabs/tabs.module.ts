import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TabsPageRoutingModule } from './tabs-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { TabsPage } from './tabs.page';
import { HomePage } from '../home/home.page';
import { CartPage } from '../cart/cart.page';
import { LoginPage } from '../login/login.page';
@NgModule({
  entryComponents: [],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    HttpClientModule,
    Ng2SearchPipeModule,
  ],
  declarations: [TabsPage, HomePage, CartPage, LoginPage],
})
export class TabsPageModule {}
