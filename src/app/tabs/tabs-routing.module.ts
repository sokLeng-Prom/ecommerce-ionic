import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { HomePage } from '../home/home.page';
import { CartPage } from '../cart/cart.page';
import { LoginPage } from '../login/login.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'product-detail/:id',
        loadChildren: () =>
          import('../product-detail/product-detail.module').then(
            (m) => m.ProductDetailPageModule
          ),
      },
      {
        path: 'deal',
        loadChildren: () =>
          import('../deal/deal.module').then((m) => m.DealPageModule),
      },
      {
        path: 'wishlist',
        loadChildren: () =>
          import('../wishlist/wishlist.module').then(
            (m) => m.WishlistPageModule
          ),
      },
      {
        path: 'store',
        loadChildren: () =>
          import('../store/store.module').then((m) => m.StorePageModule),
      },

      {
        path: 'account',
        loadChildren: () =>
          import('../account/account.module').then((m) => m.AccountPageModule),
      },
      {
        path: 'cart',
        component: CartPage,
      },
      {
        path: 'login',
        component: LoginPage,
      },
      {
        path: 'home',
        component: HomePage,
      },
    ],
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
