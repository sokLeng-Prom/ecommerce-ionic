import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { parseI18nMeta } from '@angular/compiler/src/render3/view/i18n/meta';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { identifierModuleUrl } from '@angular/compiler';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { DataService } from '../shared/data-service.server.service';
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {
  constructor(
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dataService: DataService
  ) {}

  productData: any;
  quantity: number = 1;
  productArray: any[] = [];

  postID;
  postQuantity;

  ngOnInit() {
    this.inquiryProduct();
    // this.inquiryVendor();
  }

  inquiryProduct() {
    const id: number = this.activatedRoute.snapshot.params.id;
    // console.log(id);

    const url = environment.url + '/products/' + id;

    this.http.get(url).subscribe((data) => {
      //process the json data
      console.log(data);
      this.productData = data;
    });
  }

  addToCart(quantity) {
    /**  Old Code */
    // this.quantity = quantity;
    // this.inquiryVendor(quantity);
    // console.log(this.quantity)
    // Swal.fire('Confirmation', 'You have created an order', 'success')
    /**
     * New code
     */
    const obj = {
      userId: 2,
      productId: this.productData.id,
      qty: this.quantity,
    };
    console.log('obj==>', obj);
    this.dataService.createCart(obj).then((res) => {
      if (res['date']) {
        Swal.fire('Confirmation', 'You have created an order', 'success');
        this.router.navigateByUrl('tabs/login');
      } else {
        Swal.fire(
          'Confirmation',
          'You have have fail created an order',
          'error'
        );
      }
    });
    // this.productArray.push();
    //   console.log(this.productArray)
  }

  // inquiryVendor(quantity) {
  //   const id: number = this.activatedRoute.snapshot.params.id;
  //   const cartURL = environment.url + '/carts';
  //   const body = {
  //     userId: 2,
  //     date: 2021-8-17,
  //     products: [{
  //       productId: id,
  //       quantity: this.quantity,
  //     }]
  //   };
  //   console.log('body==>',body);
  //   // return true;
  //   this.http.post<PostProduct>(cartURL, {
  //     body:JSON.stringify(body)
  //   }).subscribe(data => {
  //     this.postID = data.id;
  //     this.postQuantity = data.quantity;
  //   })
  //   this.productArray.push(body);
  //   console.log(this.productArray)

  // }

  decreaseQuantity() {
    if (this.quantity > 1) {
      this.quantity -= 1;
    }
  }

  increaseQuantity() {
    this.quantity += 1;
  }

  loginNavigation() {
    this.router.navigateByUrl('tabs/login');
  }
}

interface PostProduct {
  id: any;
  productID: number;
  quantity: number;
}
